from datetime import date, time
import datetime
from pathlib import Path
import json
from XL_Scraping import*
#--XỬ LÝ LƯU TRỮ
thuMucDuLieu = Path("./Du_lieu")
thuMucCongTy = Path("Du_lieu/Cong_ty")
thuMucHTML = Path("Du_lieu/HTML")
thuMucNguoiTrongRau = Path("Du_lieu/Nguoi_trong_rau")

#I CAC HAM XU LY LUU TRU 
#-----Doc khung html giao dien chinh
def docKhungHTML():
        chuoiHTML = ""       
        duongDan = Path("Du_lieu/HTML/Khung.html")
        chuoiHTML = duongDan.read_text("utf-8")
        return chuoiHTML
#-----/Doc khung html giao dien chinh
#----Doc khung htm Doc_Khung_HTML_Nguoi_trong_rau
def Doc_Khung_HTML_Nguoi_trong_rau():
        chuoiHTML = ""       
        duongDan = Path("Du_lieu/HTML/KhungHTML_Nguoi_trong_rau.html")
        chuoiHTML = duongDan.read_text("utf-8")
        return chuoiHTML
#----/Doc khung htm Doc_Khung_HTML_Nguoi_trong_rau
#---Doc_Bieu_do_Do_am_Nhiet_do_Khong_khi
def Doc_Bieu_do_Do_am_Nhiet_do_Khong_khi():
        chuoiHTML = ""       
        duongDan = Path("Du_lieu/HTML/Thong_so_Do_am.html")
        chuoiHTML = duongDan.read_text("utf-8")
        return chuoiHTML   
#---/Doc_bieu_do_Do_am
#---Doc_Bieu_do_Do_am_Dat
def Doc_Bieu_do_Do_am_Dat():
        chuoiHTML = ""       
        duongDan = Path("Du_lieu/HTML/Thong_so_Do_am_Dat.html")
        chuoiHTML = duongDan.read_text("utf-8")
        return chuoiHTML   
#---/Doc_bieu_do_Do_am_Dat
#---Doc_Du_bao_Thoi_tiet
def Doc_Du_bao_Thoi_tiet():
        chuoiHTML = ""       
        duongDan = Path("Du_lieu/HTML/Du_bao_Thoi_tiet.html")
        chuoiHTML = duongDan.read_text("utf-8")
        return chuoiHTML   
#---/Doc_Du_bao_Thoi_tiet
#-----Doc cong ty
def Doc_cong_ty():
    Cong_ty = {}
    Duong_dan = Path("Du_lieu/Cong_ty/VGP.json")
    Chuoi_Json = Duong_dan.read_text("utf-8")
    Cong_ty = json.loads(Chuoi_Json)
    return Cong_ty
#-----/Doc cong ty
#----Doc nguoi trong rau
def Doc_Danh_sach_Nguoi_trong_rau():
        Danh_sach_Nguoi_trong_rau = []
        for Duong_dan in thuMucNguoiTrongRau.glob("*.json"):
                Chuoi_Json = Duong_dan.read_text("utf-8")
                ntr = json.loads(Chuoi_Json)
                Danh_sach_Nguoi_trong_rau.append(ntr)
        return Danh_sach_Nguoi_trong_rau
#----/Doc nguoi trong rau

#--Doc gia nong san
def Doc_Danh_sach_gia():
	return Xu_ly_Lay_gia_rau()
#--/Doc gia nong san 


#/I CAC HAM XU LY LUU TRU

# /II CAC HAM XU LY NGHIEP VU
#----Xu_ly_Tao_Danh_sach_Rau_Cong_ty
def Xu_ly_Tao_Danh_sach_Rau_Cong_ty(Ma_Loai_rau=""):
        Danh_sach_Rau_Cong_ty = []
        Cong_ty = Doc_cong_ty()
        if Ma_Loai_rau =="":
                for Loai_rau in Cong_ty["Danh_sach_Loai_rau"]:             
                        Danh_sach_Rau_Cong_ty.append(Loai_rau)
        else:
                for Loai_rau in Cong_ty["Danh_sach_Loai_rau"]:
                        if Loai_rau["Ma_Loai_rau"] == Ma_Loai_rau:             
                                Danh_sach_Rau_Cong_ty.append(Loai_rau)
                                
        return Danh_sach_Rau_Cong_ty

#----/Xu_ly_Tao_Danh_sach_Rau

#---Xu_ly_Dang_nhap_Nguoi_trong_rau
def Xu_ly_Dang_nhap_Nguoi_trong_rau(Ten_Dang_nhap, Mat_khau):
        Nguoi_trong_rau = {}
        Danh_sach_Nguoi_trong_rau = Doc_Danh_sach_Nguoi_trong_rau()
        for ntr in Danh_sach_Nguoi_trong_rau:
                if(ntr["Ten_Dang_nhap"] == Ten_Dang_nhap and ntr["Mat_khau"]==Mat_khau):
                        Nguoi_trong_rau = ntr
                        break
        return Nguoi_trong_rau
#---/Xu_ly_Dang_nhap_Khach_hang

#---Xu_ly_Tim_kiem_Gia_rau
def Xu_ly_TimKiem_Gia_rau(Danh_sach_Gia_rau, TuKhoa):
	dstk = []
	TuKhoa = TuKhoa.upper()
	for rau in Danh_sach_Gia_rau:
		if TuKhoa in rau["Ten"].upper() or TuKhoa in rau["Gia"] :
			dstk.append(rau)
	return dstk 
#---/Xu_ly_Tim_kiem_gia_rau


#/II CAC HAM XU LY NGHIEP VU


# III CAC HAM XU LY GIAO DIEN
#-----Giao dien loai rau
def Chuoi_HTML_Danh_sach_Loai_Rau():        
        Cong_ty = Doc_cong_ty()
        chuoiHTML = ""
        for Loai_rau in Cong_ty["Danh_sach_Loai_rau"]:               
                        chuoiHTML += f'''  
                                       <a class="nav-link"  href="/loai-rau?Ma_Loai_rau={Loai_rau["Ma_Loai_rau"]}" >{Loai_rau["Ten_Loai_rau"]}</a>
                                           
                        
                        '''
        return chuoiHTML
#----Giao dien rau
def Chuoi_HTML_Danh_sach_Rau(Danh_sach_Loai_rau):      
        chuoiHTML = ""
        for Loai_rau in Danh_sach_Loai_rau:
                for Rau in Loai_rau["Danh_sach_Rau"]:
                        chuoiHTML += f'''
                                <div class="col-lg-4 col-md-6 special-grid drinks">
									<div class="gallery-single fix">
										<img src="../Media/Cong_ty/Rau/{Rau["Hinh"]}" class="img-fluid" alt="{Rau["Hinh"]}" style="width:400px;height:400px;" />
										<div class="why-text">
											<h5>{Rau["Ten_rau"]}</h5>
											<p>Thời vụ: {Rau["Thoi_vu_trong"]}</p>
											<h5>{Rau["Thoi_gian_thu_hoach"]}</h5>
                                                                                        <div> 
                                                                                        <a href="#" class="btn btn-danger"> TRỒNG RAU </a>   
                                                                        </div>
										</div>
									</div>
                                                                        
								</div>
                        '''
        return chuoiHTML

#----/Giao dien rau
#---Giao dien rau cua nguoi trong
def Chuoi_HTML_Danh_sach_rau_Nguoi_trong(Danh_sach_Rau_trong):
        chuoiHTML =""
        for Rau_trong in Danh_sach_Rau_trong:
               chuoiHTML += f'''
                <div class="col-lg-4 col-md-6 special-grid drinks">
									<div class="gallery-single fix">
										<img src="../Media/Cong_ty/Rau/{Rau_trong["Rau_trong"]["Ma_rau"]}.jpg" class="img-fluid" alt="{Rau_trong["Rau_trong"]["Ma_rau"]}.jpg" style="width:400px;height:400px;" />
										<div class="why-text">
											<h5>VƯỜN RAU: {Rau_trong["Rau_trong"]["Ten_rau"]}</h5>
											<h5>Thời gian thu hoạch dự kiến còn: xx ngày</h5>
											<h5>Ngày trồng: {Rau_trong["Ngay_trong"]}</h5>
                                                                                        <div> 
                                                                                        <a href="#" class="btn btn-danger"> THĂM VƯỜN RAU </a>   
                                                                        </div>
										</div>
									</div>
                                                                        
								</div>
               '''              
                
        return chuoiHTML
        
#---/Giao dien rau cua nguoi trong
#---Giao dien menu Danh_sach_Loai_rau va Danh_sach_Rau
def Chuoi_HTML_Danh_sach_Loai_rau_Rau(Danh_sach_Loai_rau):
        chuoiHTML = ""
        chuoiHTML += f'''
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="index.html">
					<img src="../Media/images/logo.png" alt="" />
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item active"><a class="nav-link" href="/">TRANG CHỦ</a></li>					
						<li class="nav-item"><a class="nav-link" href="/">GIỚI THIỆU</a></li>
						<li class="nav-item"><a class="nav-link" href="/xem-gia-nong-san">GÍA NÔNG SẢN</a></li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">HƯỚNG DẪN SỬ DỤNG</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="reservation.html">TRỒNG RAU</a>
								<a class="dropdown-item" href="stuff.html">THIẾT BỊ</a>
								<a class="dropdown-item" href="gallery.html">CHỢ NÔNG SẢN</a>
							</div>
						</li>						
						<li class="nav-item"><a class="nav-link" href="contact.html">LIÊN HỆ</a></li>
						<li class="nav-item">
							<!--
							<a class="nav-link dropdown-toggle btn-primary" href="#" id="dropdown-a" data-toggle="dropdown">ĐĂNG NHẬP</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="blog.html">blog</a>
								<a class="dropdown-item" href="blog-details.html">blog Single</a>
							</div>
						-->
						<a class="nav-link  btn-primary" data-toggle="modal" data-target="#myModalKhach">
							ĐĂNG NHẬP
						</a>
						</li>

					</ul>
				</div>
			</div>
		</nav>
	</header>	
	<!-- End header -->
	<!--Modal login khach-->
	 <!--modal login khach-->
	 <div class="modal" id="myModalKhach" data-backdrop="false">
		<div class="modal-dialog modal-lg">
		  <div class="modal-content">
		  
			<!-- Modal Header -->
			<div class="modal-header bg-warning">
			  <h4 class="modal-title">ĐĂNG NHẬP CỦA KHÁCH HÀNG</h4>
			  <button type="button" class="close bg-danger" data-dismiss="modal">&times;</button>
			</div>
			
			<!-- Modal body -->
			<div class="modal-body">
			  <form action="/xl-dang-nha-nguoi-trong-rau" method="post">
				  <div class="imgcontainer text-center">
					<img src="../../images/khach_hang_avatar.png" style="width:200px; height:200px;" alt="Avatar" class="rounded-circle">
				  </div>
				
				  <div class="container">
					<label for="uname"><b>Username</b></label>
					<input type="text" placeholder="Enter Username" name="txtTenDangNhap" class="form-control" required>
				
					<label for="psw"><b>Password</b></label>
					<input type="password" placeholder="Enter Password" name="txtMatKhau" class="form-control" required> <br>
						
					<div><input type="submit" name="login"  value="ĐĂNG NHẬP" class="btn btn-primary orm-control"></div> <br>
					<label>
					  <input type="checkbox" checked="checked" name="remember"> Remember me
					</label>
				  </div>
				
				  <div class="container" style="background-color:#f1f1f1">
					<a href="http://php-quoc.epizy.com/Crud/dang_ky" class="btn btn-danger">ĐĂNG KÝ </a>
					<span class="psw">  <a href="#">Forgot password?</a></span>
				  </div>
				</form>
			</div>
		  </div>
		</div>
	  </div>
	<!--/modal login khach-->
	<!--/Modal login khach-->
	<!--/Start header-->

        <!-- Start Menu -->
        <!--search tool-->
        <div class="container py-5"></div>
        <div class="container py-5"></div>
        <div class="container bg-success py-3  d-flex justify-content-end" >
                <div>
                        <form class="form-inline my-2 my-lg-3">
                                <input class="form-control mr-lg-2" type="text" placeholder="Search">
                                <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Tìm kiếm</button>
                        </form>
                </div>
        </div>
        <!--/search tool-->
	<div class="menu-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2>Special Menu</h2>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
					</div>
				</div>
			</div>
			
			<div class="row inner-menu-box">
				<div class="col-3">
					<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<a class="nav-link active" href="/loai-rau?Ma_Loai_rau=">Tất cả</a>
						{Chuoi_HTML_Danh_sach_Loai_Rau()}						
					</div>
				</div>
				
				<div class="col-9">
					<div class="tab-content" id="v-pills-tabContent">
						<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
							<div class="row">
								
								{Chuoi_HTML_Danh_sach_Rau(Danh_sach_Loai_rau)}
								
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<!-- End Menu -->        
        '''
        return chuoiHTML
#---/Giao dien menu Danh_sach_Loai_rau va Danh_sach_Rau
#---Giao dien Nguoi_trong_rau_Dang_nhap
def Chuoi_HTML_Nguoi_trong_rau_Dang_nhap(Nguoi_trong_rau):
        Danh_sach_Rau_trong = Nguoi_trong_rau["Danh_sach_Rau_trong"] 
        ChuoiHTML = f'''
        <!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="index.html">
					<img src="../Media/images/logo.png" alt="" />
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
                                                <li class="nav-item active"><a class="nav-link" href="/">TRANG TRẠI </a></li>	
						<li class="nav-item"><a class="nav-link" href="index.html">CHỌN LOẠI CÂY TRỒNG MỚI</a></li>		
						
						
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">NÔNG CỤ</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="reservation.html">TRỒNG RAU</a>
								<a class="dropdown-item" href="stuff.html">THIẾT BỊ</a>
								<a class="dropdown-item" href="gallery.html">CHỢ NÔNG SẢN</a>
							</div>
						</li>						
						<li class="nav-item"><a class="nav-link" href="contact.html">LIÊN HỆ</a></li>
						<li class="nav-item">
							<!--
							<a class="nav-link dropdown-toggle btn-primary" href="#" id="dropdown-a" data-toggle="dropdown">ĐĂNG NHẬP</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="blog.html">blog</a>
								<a class="dropdown-item" href="blog-details.html">blog Single</a>
							</div>
						-->
						<a class="nav-link  btn-primary" href="/Nguoi-trong-rau/thoat-dang-nhap">
                                                
							THOÁT ĐĂNG NHẬP <span class="badge badge-warning">{Nguoi_trong_rau["Ten_Dang_nhap"]}</span>
						</a>
						</li>

					</ul>
				</div>
			</div>
		</nav>
	</header>	
	<!-- End header -->
	<!--Modal login khach-->
	 <!--modal login khach-->
	 <div class="modal" id="myModalKhach" data-backdrop="false">
		<div class="modal-dialog modal-lg">
		  <div class="modal-content">
		  
			<!-- Modal Header -->
			<div class="modal-header bg-warning">
			  <h4 class="modal-title">ĐĂNG NHẬP CỦA KHÁCH HÀNG</h4>
			  <button type="button" class="close bg-danger" data-dismiss="modal">&times;</button>
			</div>
			
			<!-- Modal body -->
			<div class="modal-body">
			  <form action="/xl-dang-nha-nguoi-trong-rau" method="post">
				  <div class="imgcontainer text-center">
					<img src="../../images/khach_hang_avatar.png" style="width:200px; height:200px;" alt="Avatar" class="rounded-circle">
				  </div>
				
				  <div class="container">
					<label for="uname"><b>Username</b></label>
					<input type="text" placeholder="Enter Username" name="txtTenDangNhap" class="form-control" required>
				
					<label for="psw"><b>Password</b></label>
					<input type="password" placeholder="Enter Password" name="txtMatKhau" class="form-control" required> <br>
						
					<div><input type="submit" name="login"  value="ĐĂNG NHẬP" class="btn btn-primary orm-control"></div> <br>
					<label>
					  <input type="checkbox" checked="checked" name="remember"> Remember me
					</label>
				  </div>
				
				  <div class="container" style="background-color:#f1f1f1">
					<a href="http://php-quoc.epizy.com/Crud/dang_ky" class="btn btn-danger">ĐĂNG KÝ </a>
					<span class="psw">  <a href="#">Forgot password?</a></span>
				  </div>
				</form>
			</div>
			
		   
			
		  </div>
		</div>
	  </div>
	<!--/modal login khach-->
	<!--/Modal login khach-->
	
	<!-- Start Menu -->
		 <!-- Start Menu -->
        <!--search tool-->
        <div class="container py-5"></div>
        <div class="container py-5"></div>
        
        <div class="container-fluid bg-success py-3 px-5 d-flex justify-content-end" >
                <div>
                        <form class="form-inline my-2 my-lg-3">
                                <input class="form-control mr-lg-2" type="text" placeholder="Search">
                                <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Tìm kiếm</button>
                        </form>
                </div>
        </div>
        <!--/search tool-->
	<div class="menu-box">
		<div class="container-fluid">			
			<div class="row">
				<div class="col-4 bg-warning">
					<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<a class="nav-link active" href="#">THÔNG SỐ KỸ THUẬT TRANG TRẠI</a>
                                                <div class="bg-primary">
                                                        <a class="nav-link" href="#">ĐỘ ẨM - NHIỆT ĐỘ KHÔNG KHÍ TRANG TRẠI</a>
                                                        <div>
                                                               {Doc_Bieu_do_Do_am_Nhiet_do_Khong_khi()}
                                                        </div>
                                                </div>
                                                 <div class="">
                                                      <div class="bg-success">  <a class="nav-link" href="#">ĐỘ ẨM ĐẤT TRANG TRẠI</a> </div>
                                                        <div>
                                                                {Doc_Bieu_do_Do_am_Dat()}
                                                        </div>
                                                </div>	  	        					 <div class="">
                                                      <div class="bg-danger">  <a class="nav-link" href="#">DỰ BÁO THỜI TIẾT</a> </div>
                                                        <div>
                                                                {Doc_Du_bao_Thoi_tiet()}
                                                        </div>
                                                </div>	  		
					</div>
				</div>				
				<div class="col-8">
					<div class="tab-content" id="v-pills-tabContent">
						<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
							<div class="row">
								<!--Danh sach cac laoi rau da trong -->
								{Chuoi_HTML_Danh_sach_rau_Nguoi_trong(Nguoi_trong_rau["Danh_sach_Rau_trong"])}					
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<!-- End Menu -->       
	<!-- End Menu -->
	
	<!-- Start Gallery -->
	<div class="gallery-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2>Gallery</h2>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
					</div>
				</div>
			</div>
			<div class="tz-gallery">
				<div class="row">					
					xxxxxxxxxxxxxxxxxxx====>Gallery

					chuoiHTML
				</div>
			</div>
		</div>
	</div>
	<!-- End Gallery -->
	
	<!-- Start Customer Reviews -->
	<div class="customer-reviews-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2>Customer Reviews</h2>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 mr-auto ml-auto text-center">
					<div id="reviews" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner mt-4">
							<div class="carousel-item text-center active">
								<div class="img-box p-1 border rounded-circle m-auto">
									<img class="d-block w-100 rounded-circle" src="../../images/quotations-button.png" alt="">
								</div>
								<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Paul Mitchel</strong></h5>
								<h6 class="text-dark m-0">Web Developer</h6>
								<p class="m-0 pt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
							</div>
							<div class="carousel-item text-center">
								<div class="img-box p-1 border rounded-circle m-auto">
									<img class="d-block w-100 rounded-circle" src="../../images/quotations-button.png" alt="">
								</div>
								<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Steve Fonsi</strong></h5>
								<h6 class="text-dark m-0">Web Designer</h6>
								<p class="m-0 pt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
							</div>
							<div class="carousel-item text-center">
								<div class="img-box p-1 border rounded-circle m-auto">
									<img class="d-block w-100 rounded-circle" src="../../images/quotations-button.png" alt="">
								</div>
								<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Daniel vebar</strong></h5>
								<h6 class="text-dark m-0">Seo Analyst</h6>
								<p class="m-0 pt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
							</div>
						</div>
						<a class="carousel-control-prev" href="#reviews" role="button" data-slide="prev">
							<i class="fa fa-angle-left" aria-hidden="true"></i>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#reviews" role="button" data-slide="next">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
							<span class="sr-only">Next</span>
						</a>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Customer Reviews -->
	
	<!-- Start Contact info -->
	<div class="contact-imfo-box">
		<div class="container">
			<div class="row">
				<div class="col-md-4 arrow-right">
					<i class="fa fa-volume-control-phone"></i>
					<div class="overflow-hidden">
						<h4>Phone</h4>
						<p class="lead">
							+01 123-456-4590
						</p>
					</div>
				</div>
				<div class="col-md-4 arrow-right">
					<i class="fa fa-envelope"></i>
					<div class="overflow-hidden">
						<h4>Email</h4>
						<p class="lead">
							yourmail@gmail.com
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-map-marker"></i>
					<div class="overflow-hidden">
						<h4>Location</h4>
						<p class="lead">
							800, Lorem Street, US
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Contact info -->
	
	<!-- Start Footer -->
	<footer class="footer-area bg-f">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<h3>About Us</h3>
					<p>Integer cursus scelerisque ipsum id efficitur. Donec a dui fringilla, gravida lorem ac, semper magna. Aenean rhoncus ac lectus a interdum. Vivamus semper posuere dui.</p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Subscribe</h3>
					<div class="subscribe_form">
						<form class="subscribe_form">
							<input name="EMAIL" id="subs-email" class="form_input" placeholder="Email Address..." type="email">
							<button type="submit" class="submit">SUBSCRIBE</button>
							<div class="clearfix"></div>
						</form>
					</div>
					<ul class="list-inline f-social">
						<li class="list-inline-item"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Contact information</h3>
					<p class="lead">Ipsum Street, Lorem Tower, MO, Columbia, 508000</p>
					<p class="lead"><a href="#">+01 2000 800 9999</a></p>
					<p><a href="#"> info@admin.com</a></p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Opening hours</h3>
					<p><span class="text-color">Monday: </span>Closed</p>
					<p><span class="text-color">Tue-Wed :</span> 9:Am - 10PM</p>
					<p><span class="text-color">Thu-Fri :</span> 9:Am - 10PM</p>
					<p><span class="text-color">Sat-Sun :</span> 5:PM - 10PM</p>
				</div>
			</div>
		</div>
		
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name">All Rights Reserved. &copy; 2018 <a href="#">Live Dinner Restaurant</a> Design By : 
					<a href="https://html.design/">html design</a></p>
					</div>
				</div>
			</div>
		</div>
		
	</footer>
	<!-- End Footer -->
	
	<a href="#" id="back-to-top" title="Back to top" style="display: none;"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>
        '''
        return ChuoiHTML
#---/Giao dien Nguoi_trong_rau_Dang_nhap
#---Giao dien du bao thoi tiet
def Chuoi_HTML_Du_bao_Thoi_tiet(ChuoiJSON_Thoi_tiet):
	chuoiHTML = f'''
	<div>
		<p>Kiểu thời tiết: {ChuoiJSON_Thoi_tiet["main"]} </p>
		<p>Chi tiết: {ChuoiJSON_Thoi_tiet["des"]} </p>
		<p>Tốc độ gió: {ChuoiJSON_Thoi_tiet["wind_speed"]} </p>
		<p>Mật độ mây: {ChuoiJSON_Thoi_tiet["cloud"]} </p>
		<h5>UVI: {ChuoiJSON_Thoi_tiet["uvi"]} </h5>
		<img src="../Media/Thoi_tiet/{ChuoiJSON_Thoi_tiet["icon"]}.png" class="img-fluid" alt="{ChuoiJSON_Thoi_tiet["icon"]}.png" style="width:200px;height:200px;" />
	</div>
	'''
	return chuoiHTML
#---/Giao dien du bao thoi tiet

#--Giao dien gia rau
def Chuoi_HTML_Gia_rau(Danh_sach_Gia_rau, TuKhoa=''):
	chuoiHTML = ''
	chuoiHTML_Gia_rau = ''
	for rau in Danh_sach_Gia_rau:
		chuoiHTML_Gia_rau += f'''
		<tr>
			<td>{rau["Ten"]}</td>
			<td>{rau["Gia"]}</td>        
      	</tr>      
		'''
	chuoiHTML = f'''
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="index.html">
					<img src="../Media/images/logo.png" alt="" />
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item active"><a class="nav-link" href="/">TRANG CHỦ</a></li>					
						<li class="nav-item"><a class="nav-link" href="/">GIỚI THIỆU</a></li>
						<li class="nav-item"><a class="nav-link" href="/xem-gia-nong-san">GÍA NÔNG SẢN</a></li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">HƯỚNG DẪN SỬ DỤNG</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="reservation.html">TRỒNG RAU</a>
								<a class="dropdown-item" href="stuff.html">THIẾT BỊ</a>
								<a class="dropdown-item" href="gallery.html">CHỢ NÔNG SẢN</a>
							</div>
						</li>						
						<li class="nav-item"><a class="nav-link" href="contact.html">LIÊN HỆ</a></li>
						<li class="nav-item">
							<!--
							<a class="nav-link dropdown-toggle btn-primary" href="#" id="dropdown-a" data-toggle="dropdown">ĐĂNG NHẬP</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="blog.html">blog</a>
								<a class="dropdown-item" href="blog-details.html">blog Single</a>
							</div>
						-->
						<a class="nav-link  btn-primary" data-toggle="modal" data-target="#myModalKhach">
							ĐĂNG NHẬP
						</a>
						</li>

					</ul>
				</div>
			</div>
		</nav>
	</header>	
	<!-- End header -->
	<!--Modal login khach-->
	 <!--modal login khach-->
	 <div class="modal" id="myModalKhach" data-backdrop="false">
		<div class="modal-dialog modal-lg">
		  <div class="modal-content">
		  
			<!-- Modal Header -->
			<div class="modal-header bg-warning">
			  <h4 class="modal-title">ĐĂNG NHẬP CỦA KHÁCH HÀNG</h4>
			  <button type="button" class="close bg-danger" data-dismiss="modal">&times;</button>
			</div>
			
			<!-- Modal body -->
			<div class="modal-body">
			  <form action="/xl-dang-nha-nguoi-trong-rau" method="post">
				  <div class="imgcontainer text-center">
					<img src="../../images/khach_hang_avatar.png" style="width:200px; height:200px;" alt="Avatar" class="rounded-circle">
				  </div>
				
				  <div class="container">
					<label for="uname"><b>Username</b></label>
					<input type="text" placeholder="Enter Username" name="txtTenDangNhap" class="form-control" required>
				
					<label for="psw"><b>Password</b></label>
					<input type="password" placeholder="Enter Password" name="txtMatKhau" class="form-control" required> <br>
						
					<div><input type="submit" name="login"  value="ĐĂNG NHẬP" class="btn btn-primary orm-control"></div> <br>
					<label>
					  <input type="checkbox" checked="checked" name="remember"> Remember me
					</label>
				  </div>
				
				  <div class="container" style="background-color:#f1f1f1">
					<a href="http://php-quoc.epizy.com/Crud/dang_ky" class="btn btn-danger">ĐĂNG KÝ </a>
					<span class="psw">  <a href="#">Forgot password?</a></span>
				  </div>
				</form>
			</div>
		  </div>
		</div>
	  </div>
	<!--/modal login khach-->
	<!--/Modal login khach-->
	<!--/Start header-->
	 <!--search tool-->
        <div class="container py-5"></div>
        <div class="container py-5"></div>
        <div class="container bg-success py-3  d-flex justify-content-end" >
                <div > 
                        <form class="form-inline my-2 my-lg-3" action="/tim-kiem-gia-nong-san" method="post">
								<div class = "ui-widget">
								
                                <input class="form-control mr-lg-2" type="text" name="txtTimKiem" value="{TuKhoa}" placeholder="Search" id="tags-rau">
                                 </div>
								<button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Tìm kiếm</button>
                        </form>
                </div>
        </div>
        <!--/search tool-->
	<div class='container py-5'> 
			 <table class="table table-hover">
    <thead>
      <tr>
        <th>Ten Rau</th>
        <th>Gia rau</th>       
      </tr>
    </thead>
    <tbody>
      {chuoiHTML_Gia_rau}     
    </tbody>
  </table>
		</div>
	
	'''
	return chuoiHTML
#--/Giao dien gia rau
# /III CAC HAM XU LY GIAO DIEN     
   